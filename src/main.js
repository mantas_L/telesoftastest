import Vue from 'vue'
import App from './App.vue'

// Global components
import BaseButton from "./components/base/BaseButton";
import BaseInput from "./components/base/BaseInput";
import BaseNode from "./components/base/BaseNode";

Vue.component('BaseButton', BaseButton);
Vue.component('BaseInput', BaseInput);
Vue.component('BaseNode', BaseNode);

Vue.config.productionTip = false;

new Vue({
	render: h => h(App),
}).$mount('#app')
